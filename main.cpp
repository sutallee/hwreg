#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "namespace.h"
#include "cpreproc.h"

/*
	The plan for "hwreg" is that it take options which specify
	the type of symbols to output. E.g.:
		* GNU Assembler format (e.g. .set SYMBOL, EXPRESSION
		* C++ class scoped constants
		* C++ namespace scoped constants
		* C global enumerations
	C++ class namespace constants are prefered since the "using"
	keyword can be used to shorten names and make source code
	more readable.

	Based on the "command-line" option specified, an appropriate
	output object will be instantiated and used to produce output
	from the parsed input file.

	To speed processing in the case where many output formats
	need to be produced from a single input file, it would be
	desireable to be able to specify several output formats and
	file-names one the command line. This would speed up code generation
	by not spinning up a new process for each conversion, and by only
	needing to parse the input file one time. I am not sure what
	affect if any this approach would have on makefile dependency
	checking. However, with the exception of disk-space usage,
	it may be desireable to *always* produce all possible output
	files.

`.set SYMBOL, EXPRESSION'
=========================

   Set the value of SYMBOL to EXPRESSION.  This changes SYMBOL's value
and type to conform to EXPRESSION.  If SYMBOL was flagged as external,
it remains flagged (*note Symbol Attributes::.).

   You may `.set' a symbol many times in the same assembly.

   If you `.set' a global symbol, the value stored in the object file
is the last value stored into it.

   The syntax for `set' on the HPPA is `SYMBOL .set EXPRESSION'.




*/

int yyparse(void);

class CompileApi {
	public:
		virtual void	compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) throw()=0;
	};

class NamespaceCompiler : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class CPreProcessCompiler : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

static void printHelp(const char* programName){
	const char	helpFormat[]=	"%s [options] [-o outputFile]"
								" inputFile [inputFile,..]\n"
								"This program compiles a register description <inputFile> and\n"
								"generates a header file that describes the registers.\n"
								"options:\n"
								" -h\n"
								"   Displays this help message.\n"
								" -c <copyright_notice>\n"
								"   Specifies the path to a file which contains a comment that\n"
								"   is to be prepended to each generated file. This file is\n"
								"   typically a copyright notice.\n"
								" -t <outputType>\n"
								"   The outputType may be one of:\n"
								"     - namespace (DEFAULT)\n"
								"       Generates a C++ header file using namespaces.\n"
								"     - cpp\n"
								"       Generates C pre-processor header file with long names.\n"
								" -e\n"
								"   Use enums for constants, otherwise 'constexpr Reg' will be used.\n"
								;
	fprintf(	stderr,
				helpFormat,
				programName
				);
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							);

static const char*	copyrightNotice=0;
static bool			useEnums	= false;

int main(int argc,char *argv[]){
	const char*			outputFileName=0;
	int					opt;
	bool				useStdIn=false;
	CPreProcessCompiler	cppCompiler;
	NamespaceCompiler	namespaceCompiler;
	CompileApi*			compiler = &namespaceCompiler;
	extern FILE*		yyin;

	while((opt = getopt(argc,argv,"ec:it:o:h")) != -1){
		switch(opt){
			case 'c':
				copyrightNotice	= optarg;
				break;
			case 't':
				if(strcmp("namespace",optarg) == 0){
					compiler	= &namespaceCompiler;
					}
				else if(strcmp("cpp",optarg) == 0){
					compiler	= &cppCompiler;
					}
				else {
					fprintf(	stderr,
								"%s: unrecognized output type %s\n",
								argv[0],
								optarg
								);
					exit(1);
					}
				break;
			case 'o':
				outputFileName	= optarg;
				break;
			case 'h':
				printHelp(argv[0]);
				exit(1);
			case 'i':
				useStdIn	= true;
				break;
			case 'e':
				useEnums	= true;
				break;
			case ':':
				fprintf(stderr,"%s: option %c requires an argument\n",argv[0],optopt);
				exit(1);
			case '?':
				fprintf(stderr,"%s: unknown option: %c\n",argv[0],optopt);
				exit(1);
			}
		}

	FILE*	outputFile;
	if(outputFileName){
		outputFile	= fopen(outputFileName,"w");
		if(!outputFile){
			fprintf(stderr,"%s: can't open output file!\n",argv[0]);
			perror(argv[0]);
			exit(1);
			}
		}
	else {
		outputFile	= stdout;
		}

	if(useStdIn){
		yyin	= stdin;
		compileStream(argv[0],*compiler,outputFile);
		}
	else if(optind >= argc){
		fprintf(	stderr,
					"%s: missing input file(s)!\n",
					argv[0]
					);
		printHelp(argv[0]);
		}

	for(int i=optind;i<argc;++i){
		yyin	= fopen(argv[i],"r");
		if(!yyin){
			fprintf(	stderr,
						"%s: can't open input file %s\n",
						argv[0],
						argv[i]
						);
			perror(argv[0]);
			exit(1);
			}
		compileStream(argv[0],*compiler,outputFile);
		}
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							){
	int	status	= yyparse();

	if(status){
		fprintf(	stderr,
					"%s: yyparse() failed! : %d\n",
					progName,
					status
					);
		}
	else{

		if(copyrightNotice){
			FILE*	cn	= fopen(copyrightNotice,"r");
			if(!cn){
				perror("Trying to open copyright notice file failed:");
				fprintf(	stderr,
							"%s: cannot open copyright notice file %s.\n",
							progName,
							copyrightNotice
							);
				return;
				}
	
			char	buffer[1024];
			char*	p;
			while((p=fgets(buffer,sizeof(buffer)-1,cn))){
				fputs(p,outputFile);
				}
			}

		compiler.compile(*theModuleDescList,outputFile);
		delete theModuleDescList;
		theModuleDescList	= 0;
		}
	}

void	NamespaceCompiler::compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) throw(){
	NamespaceVisit	tv(*outputFile,useEnums);
	parseNodeList.accept(tv);
	}

void	CPreProcessCompiler::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	CPreProcVisit	tv(*outputFile);
	parseNodeList.accept(tv);
	}

