#include <stdlib.h>
#include <stdio.h>
#include "cpreproc.h"
#include "oscl/compiler/types.h"
#include "oscl/macros/bits.h"

CPreProcVisit::CPreProcVisit() throw():
		_bitNumber(0),
		_outputFile(*stdout)
		{
	}

CPreProcVisit::CPreProcVisit(FILE& outputFile) throw():
		_bitNumber(0),
		_outputFile(outputFile)
		{
	}

void	CPreProcVisit::printStack() throw(){
	Oscl::Queue<Oscl::Strings::Node>	tmp;
	Oscl::Strings::Node*				node;
	while((node=_stack.pop())){
		tmp.push(node);
		}
	while((node=tmp.pop())){
		fprintf(&_outputFile,"%s",node->getString());
		_stack.push(node);
		}
	}

void	CPreProcVisit::preVisit(ModuleDesc& moduleDesc) throw(){
	fprintf(&_outputFile,"#ifndef _%s_\n",moduleDesc.getPrefix().getString());
	fprintf(&_outputFile,"#define _%s_\n",moduleDesc.getPrefix().getString());
	}

void	CPreProcVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	CPreProcVisit::preVisit(HardwareValueDesc& hardwareValueDesc) throw(){
	}

void	CPreProcVisit::preVisit(IncludeDesc& incDesc) throw(){
	}

void	CPreProcVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	Oscl::Strings::Node*			node	= new Oscl::Strings::Node(namespaceDesc.getPrefix());
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);
	}

void	CPreProcVisit::preVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw(){
	}

void	CPreProcVisit::preVisit(ValueDesc& valueDesc) throw(){
	if(_fstack.first()){
		fprintf(&_outputFile,"#define ");
		printStack();
		fprintf(&_outputFile,"Value_%s	0x%X\n",
				valueDesc.getSymbol().getString(),
				valueDesc.getValue()
				);
		fprintf(&_outputFile,"#define ");
		printStack();
		fprintf(&_outputFile,"ValueMask_%s	0x%8.8lX\n",
				valueDesc.getSymbol().getString(),
				(unsigned long)(valueDesc.getValue()<<_bitNumber)
				);
		}
	else {
		fprintf(&_outputFile,"#define ");
		printStack();
		fprintf(&_outputFile,"%s	%d\n",
				valueDesc.getSymbol().getString(),
				valueDesc.getValue()
				);
		}
	}

void	CPreProcVisit::preVisit(RegisterDesc& regDesc) throw(){
	Oscl::Strings::Node*			node	= new Oscl::Strings::Node(regDesc.getSymbol());
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);
	}

void	CPreProcVisit::preVisit(FieldDesc& fieldDesc) throw(){
	Oscl::Strings::Node*			node	= new Oscl::Strings::Node(fieldDesc.getSymbol());
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);

	_bitNumber		+= fieldDesc.getBitNumber();
	FieldElement*	fe	= new FieldElement(fieldDesc.getFieldWidth());
	if(!fe){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_fstack.push(fe);

	fprintf(&_outputFile,"#define ");
	printStack();
	fprintf(&_outputFile,"Lsb	%u\n", _bitNumber);

	unsigned	fieldwidth	= _fstack.first()->_fieldWidth;
	unsigned long	fmask;
	if(fieldwidth < 32){
		fmask	= OsclMacroFieldMask(_fstack.first()->_fieldWidth)<<_bitNumber;
		}
	else if(fieldwidth == 32){
		fmask	= ~0L;
		}
	else {
		fprintf(&_outputFile,"error: ");
		printStack();
		fprintf(&_outputFile,"Field width %u exceeded 32 bits.\n",fieldwidth);
		fprintf(stderr,"Field exceeded 32 bits.\n");
		exit(1);
		}
	fprintf(&_outputFile,"#define ");
	printStack();
	fprintf(&_outputFile,"FieldMask	0x%8.8lX\n", fmask);
	}

void	CPreProcVisit::preVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw(){
	}

void	CPreProcVisit::preVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw(){
	}

void	CPreProcVisit::preVisit(FieldSeqList& fieldSeqList) throw(){
	}

void	CPreProcVisit::preVisit(StructDesc& structDesc) throw(){
	}

void	CPreProcVisit::preVisit(UnionDesc& unionDesc) throw(){
	}

void	CPreProcVisit::postVisit(ModuleDesc& moduleDesc) throw(){
	fprintf(&_outputFile,"#endif\n");
	}

void	CPreProcVisit::postVisit(IncludeDesc& incDesc) throw(){
	}

void	CPreProcVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on namespace stack pop\n");
		exit(1);
		}
	}

void	CPreProcVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	CPreProcVisit::postVisit(HardwareValueDesc& hardwareValueDesc) throw(){
	}

void	CPreProcVisit::postVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw(){
	}

void	CPreProcVisit::postVisit(ValueDesc& valueDesc) throw(){
	}

void	CPreProcVisit::postVisit(RegisterDesc& regDesc) throw(){
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on register stack pop\n");
		exit(1);
		}
	}

void	CPreProcVisit::postVisit(FieldDesc& fieldDesc) throw(){
	_bitNumber		-= fieldDesc.getBitNumber();
	FieldElement*	fe	= _fstack.pop();
	if(fe) {
		delete fe;
		}
	else{
		fprintf(stderr,"Logic error on field element stack pop\n");
		exit(1);
		}
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on field stack pop\n");
		exit(1);
		}
	}

void	CPreProcVisit::postVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw(){
	}

void	CPreProcVisit::postVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw(){
	}

void	CPreProcVisit::postVisit(FieldSeqList& fieldSeqList) throw(){
	}

void	CPreProcVisit::postVisit(StructDesc& structDesc) throw(){
	}

void	CPreProcVisit::postVisit(UnionDesc& unionDesc) throw(){
	}

