#ifndef _hwregh_
#define _hwregh_
#include "oscl/queue/queue.h"
#include "oscl/strings/dynamic.h"
#include "oscl/tree/node.h"

class ModuleDesc;
class IncludeDesc;
class NamespaceDesc;
class ValueDesc;
class RegisterDesc;
class FieldDesc;
class HardwareValueDesc;
class HardwareRegisterDesc;
class FieldSeqFieldDesc;
class FieldSeqValueDesc;
class FieldSeqList;
class ParseNodeList;
class StructDesc;
class UnionDesc;

extern ParseNodeList*	theModuleDescList;

class ParseElementVisitor {
	public:
		virtual ~ParseElementVisitor(){}
	public:
		virtual void	preVisit(ModuleDesc& moduleDesc) throw()=0;
		virtual void	preVisit(ParseNodeList& parseNodeList) throw()=0;
		virtual void	preVisit(IncludeDesc& includeDesc) throw()=0;
		virtual void	preVisit(NamespaceDesc& namespaceDesc) throw()=0;
		virtual void	preVisit(HardwareValueDesc& hardwareValueDesc) throw()=0;
		virtual void	preVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw()=0;
		virtual void	preVisit(ValueDesc& valueDesc) throw()=0;
		virtual void	preVisit(RegisterDesc& regDesc) throw()=0;
		virtual void	preVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw()=0;
		virtual void	preVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw()=0;
		virtual void	preVisit(FieldSeqList& fieldSeqList) throw()=0;
		virtual void	preVisit(FieldDesc& fieldDesc) throw()=0;
		virtual void	preVisit(StructDesc& structDesc) throw()=0;
		virtual void	preVisit(UnionDesc& unionDesc) throw()=0;
	public:
		virtual void	postVisit(ModuleDesc& moduleDesc) throw()=0;
		virtual void	postVisit(ParseNodeList& parseNodeList) throw()=0;
		virtual void	postVisit(IncludeDesc& includeDesc) throw()=0;
		virtual void	postVisit(NamespaceDesc& namespaceDesc) throw()=0;
		virtual void	postVisit(HardwareValueDesc& hardwareValueDesc) throw()=0;
		virtual void	postVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw()=0;
		virtual void	postVisit(ValueDesc& valueDesc) throw()=0;
		virtual void	postVisit(RegisterDesc& regDesc) throw()=0;
		virtual void	postVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw()=0;
		virtual void	postVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw()=0;
		virtual void	postVisit(FieldSeqList& fieldSeqList) throw()=0;
		virtual void	postVisit(FieldDesc& fieldDesc) throw()=0;
		virtual void	postVisit(StructDesc& structDesc) throw()=0;
		virtual void	postVisit(UnionDesc& unionDesc) throw()=0;
	};

/** */
class ParseElementApi {
	public:
		virtual ~ParseElementApi(){}
	public:
		/** */
		virtual void accept(ParseElementVisitor& visitor) throw()=0;
	};

/** */
class ParseNode : public ParseElementApi , public Oscl::QueueItem { };

/** */
class ParseNodeList : public ParseElementApi , public Oscl::Queue<ParseNode> {
	public:
		ParseNodeList() throw();
		void accept(ParseElementVisitor& visitor) throw();
	};

class TypePrimitive;
class TypeStructure;
class TypeUnion;

/** The type tree is ordered by type name.
 */
class TypeNode : public Oscl::Tree::Node<TypeNode,TypeNode> {
	public:
		/** */
		class Visitor {
			public:
				/** */
				virtual ~Visitor(){}
				/** */
				virtual void	visit(TypePrimitive& node) throw()=0;
				/** */
				virtual void	visit(TypeStructure& node) throw()=0;
				/** */
				virtual void	visit(TypeUnion& node) throw()=0;
			};
	protected:
		/** */
		Oscl::Strings::Dynamic	_typeName;
	public:
		/** */
		TypeNode(const char* typeName) throw();
		/** */
		virtual ~TypeNode() {}
		/** */
		virtual void	accept(TypeNode::Visitor& visitor) throw()=0;
	};

/** */
class StructMember : public Oscl::QueueItem {
	private:
		/** */
		TypeNode&	_type;
	public:
		/** */
		StructMember(TypeNode& type) throw();
		/** */
		void	accept(TypeNode::Visitor& visitor) throw();
	};

/** */
class UnionMember : public Oscl::QueueItem {
	private:
		/** */
		TypeNode&	_type;
	public:
		/** */
		UnionMember(TypeNode& type) throw();
		/** */
		void	accept(TypeNode::Visitor& visitor) throw();
	};

/** */
class TypePrimitive : public TypeNode {
	public:
		/** */
		TypePrimitive(const char* typeName) throw();
		/** */
		void	accept(TypeNode::Visitor& visitor) throw();
	};

/** */
class TypeStructure : public TypeNode , public Oscl::Queue<StructMember> {
	public:
		/** */
		TypeStructure(const char* typeName) throw();
		/** */
		void	accept(TypeNode::Visitor& visitor) throw();
	};

/** */
class TypeUnion : public TypeNode , public Oscl::Queue<UnionMember> {
	public:
		/** */
		TypeUnion(const char* typeName) throw();
		/** */
		void	accept(TypeNode::Visitor& visitor) throw();
	};

class ParseList : public ParseElementApi , public Oscl::Queue<ParseNode> {
	public:
		/** */
		ParseList() throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class HardwareDesc : public ParseNode {
	public:
		/** */
		HardwareDesc() throw();
	};

/** */
class NameDesc : public HardwareDesc {
	public:
		/** */
		NameDesc() throw();
	};

/** */
class IncludeDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_includeFile;
	public:
		/** */
		IncludeDesc(
			const char* prefix,
			const char* includeFile,
			const char* postfix
			) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getIncludeFile() const throw();
	};

/** */
class HardwareValueDesc : public HardwareDesc {
	private:
		/** */
		ParseNodeList*	_valueDescList;
	public:
		/** */
		HardwareValueDesc(ParseNodeList* valueDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class HardwareRegisterDesc : public HardwareDesc {
	private:
		/** */
		ParseNodeList*	_regDescList;
	public:
		/** */
		HardwareRegisterDesc(ParseNodeList* regDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class ModuleDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*	_hwDescList;
		/** */
		ParseNodeList*	_namespaceDescList;
		/** */
		ParseNodeList*	_incDescList;
		/** */
		Oscl::Strings::Dynamic	_prefix;
	public:
		/** */
		ModuleDesc(char* prefix,ParseNodeList* incDescList,ParseNodeList* hwDescList,ParseNodeList* namespaceDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getPrefix() throw();
	};

/** */
class NamespaceDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*	_parseNodeList;
		/** */
		Oscl::Strings::Dynamic	_prefix;
	public:
		/** */
		NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getPrefix() throw();
	};

/** */
class ValueDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_symbol;
		/** */
		const int			_value;
	public:
		/** */
		ValueDesc(char* symbol,int value) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&	getSymbol() throw();
		/** */
		int					getValue() throw();
	};

/** */
class FieldSeqFieldDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*	_fieldDescList;
	public:
		/** */
		FieldSeqFieldDesc(ParseNodeList* fieldDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class FieldSeqValueDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*	_valueDescList;
	public:
		/** */
		FieldSeqValueDesc(ParseNodeList* valueDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class RegisterDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*			_fieldSeqDescList;
		/** */
		Oscl::Strings::Dynamic		_symbol;
		/** */
		Oscl::Strings::Dynamic*	_type;
	public:
		/** */
		RegisterDesc(char* symbol,Oscl::Strings::Dynamic* type,ParseNodeList* fieldSeqDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&	getSymbol() throw();
		/** */
		const Oscl::Strings::Api&	getType() throw();
	};

/** */
class FieldDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*	_valueDescList;
		/** */
		Oscl::Strings::Dynamic	_symbol;
		/** */
		const int		_bitNum;
		/** */
		const int		_fieldWidth;
	public:
		/** */
		FieldDesc(char* symbol,int bitNum,int fieldWidth,ParseNodeList* valueDescList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&	getSymbol() throw();
		/** */
		int					getBitNumber() throw();
		/** */
		int					getFieldWidth() throw();
	};

/** */
class StructDesc : public ParseNode {
	public:
		/** */
		StructDesc() throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class UnionDesc : public ParseNode {
	public:
		/** */
		UnionDesc() throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

#endif
