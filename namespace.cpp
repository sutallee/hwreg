#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "namespace.h"
#include "oscl/compiler/types.h"
#include "oscl/macros/bits.h"

NamespaceVisit::NamespaceVisit(bool useEnums) throw():
		_bitNumber(0),
		_outputFile(*stdout),
		_useEnums(useEnums)
		{
	}

NamespaceVisit::NamespaceVisit(FILE& outputFile,bool useEnums) throw():
		_bitNumber(0),
		_outputFile(outputFile),
		_useEnums(useEnums)
		{
	}

void	NamespaceVisit::preVisit(ModuleDesc& moduleDesc) throw(){
	_modulePrefix		= moduleDesc.getPrefix();
	_regPrefix		= "";
	_fieldPrefix	= _modulePrefix;
	fprintf(&_outputFile,"#ifndef _%s_\n",moduleDesc.getPrefix().getString());
	fprintf(&_outputFile,"#define _%s_\n",moduleDesc.getPrefix().getString());
	}

void	NamespaceVisit::preVisit(IncludeDesc& incDesc) throw(){
	fprintf(
		&_outputFile,"#include %s\n",
		incDesc.getIncludeFile().getString()
		);
	}

void	NamespaceVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	printStack();
	fprintf(&_outputFile,"namespace %s { // Namespace description\n",namespaceDesc.getPrefix().getString());
	pushTab();
	}

void	NamespaceVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	NamespaceVisit::preVisit(HardwareValueDesc& hardwareValueDesc) throw(){
	}

void	NamespaceVisit::preVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw(){
	}

void	NamespaceVisit::preVisit(ValueDesc& valueDesc) throw(){
	if(_fstack.first()){
		printStack();

		const char*
		valueFormat	= _useEnums
			?"enum {Value_%s = 0x%X};\n"
			:"constexpr Reg Value_%s = 0x%X;\n"
			;

		fprintf(
			&_outputFile,
			valueFormat,
			valueDesc.getSymbol().getString(),
			valueDesc.getValue()
			);

		printStack();

		const char*
		valueMaskFormat	= _useEnums
			?"enum {ValueMask_%s = 0x%8.8lX};\n"
			:"constexpr Reg ValueMask_%s = 0x%8.8lX;\n"
			;

		fprintf(
			&_outputFile,
			valueMaskFormat,
			valueDesc.getSymbol().getString(),
			(unsigned long)(uint32_t)(valueDesc.getValue()<<_bitNumber)
			);
		}
	else {
		printStack();

		const char*
		valueFormat	= _useEnums
			?"enum {%s = %d};\n"
			:"constexpr Reg	%s = %d;\n"
			;
		
		fprintf(
			&_outputFile,
			valueFormat,
			valueDesc.getSymbol().getString(),
			valueDesc.getValue()
			);
		}
	}

void	NamespaceVisit::preVisit(RegisterDesc& regDesc) throw(){
	_regPrefix	= regDesc.getSymbol();
	const char*		symbol	= regDesc.getSymbol();
	const char*		type	= regDesc.getType();
	printStack();
	fprintf(&_outputFile,"namespace %s { // Register description\n",symbol);
	printStack();
	fprintf(&_outputFile,"\ttypedef %s\tReg;\n",type);
	pushTab();
	}

void	NamespaceVisit::preVisit(FieldDesc& fieldDesc) throw(){
	_fieldPrefix	= fieldDesc.getSymbol();
	_bitNumber		+= fieldDesc.getBitNumber();
	FieldElement*	fe	= new FieldElement(fieldDesc.getFieldWidth());
	if(!fe){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_fstack.push(fe);
	printStack();
	fprintf(&_outputFile,"namespace %s { // Field Description\n",_fieldPrefix.getString());
	printStack();

	const char*
	lsbFormat	= _useEnums
		?"\tenum {Lsb = %u};\n"
		:"\tconstexpr Reg Lsb = %u;\n"
		;

	fprintf(
		&_outputFile,
		lsbFormat,
		_bitNumber
		);

	unsigned	fieldwidth	= _fstack.first()->_fieldWidth;
	uint32_t	fmask;
	if(fieldwidth < 32){
		fmask	= OsclMacroFieldMask(_fstack.first()->_fieldWidth)<<_bitNumber;
		}
	else if(fieldwidth == 32){
		fmask	= ~0L;
		}
	else {
		fprintf(stderr,"Field width %u of field %s exceeded 32 bits.\n",fieldwidth, _fieldPrefix.getString());
		exit(1);
		}
	printStack();

	const char*
	fieldMaskFormat	= _useEnums
		?"\tenum {FieldMask = 0x%8.8lX};\n"
		:"\tconstexpr Reg FieldMask = 0x%8.8lX;\n"
		;

	fprintf(
		&_outputFile,
		fieldMaskFormat,
		(unsigned long)fmask
		);
	pushTab();
	}

void	NamespaceVisit::preVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw(){
	}

void	NamespaceVisit::preVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw(){
	}

void	NamespaceVisit::preVisit(FieldSeqList& fieldSeqList) throw(){
	}

void	NamespaceVisit::preVisit(StructDesc& structDesc) throw(){
	printStack();
	fprintf(&_outputFile,"typedef struct %s {\n","STRUCT");
	}

void	NamespaceVisit::preVisit(UnionDesc& unionDesc) throw(){
	printStack();
	fprintf(&_outputFile,"typedef union %s {\n","UNION");
	}

void	NamespaceVisit::postVisit(ModuleDesc& moduleDesc) throw(){
	fprintf(&_outputFile,"#endif\n");
	}

void	NamespaceVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	NamespaceVisit::postVisit(HardwareValueDesc& hardwareValueDesc) throw(){
	}

void	NamespaceVisit::postVisit(IncludeDesc& incDesc) throw(){
	}

void	NamespaceVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
	printStack();
	fprintf(&_outputFile,"}\n");
	popTab();
	}

void	NamespaceVisit::postVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw(){
	}

void	NamespaceVisit::postVisit(ValueDesc& valueDesc) throw(){
	}

void	NamespaceVisit::postVisit(RegisterDesc& regDesc) throw(){
	printStack();
	fprintf(&_outputFile,"};\n");
	popTab();
	}

void	NamespaceVisit::postVisit(FieldDesc& fieldDesc) throw(){
	_bitNumber		-= fieldDesc.getBitNumber();
	FieldElement*	fe	= _fstack.pop();
	if(fe) {
		delete fe;
		}
	else{
		fprintf(stderr,"Logic error on field element stack pop\n");
		exit(1);
		}
	printStack();
	fprintf(&_outputFile,"};\n");
	popTab();
	}

void	NamespaceVisit::postVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw(){
	}

void	NamespaceVisit::postVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw(){
	}

void	NamespaceVisit::postVisit(FieldSeqList& fieldSeqList) throw(){
	}

void	NamespaceVisit::postVisit(StructDesc& structDesc) throw(){
	printStack();
	fprintf(&_outputFile,"\t}%s;\n","STRUCT");
	}

void	NamespaceVisit::postVisit(UnionDesc& unionDesc) throw(){
	printStack();
	fprintf(&_outputFile,"\t}%s;\n","UNION");
	}


void	NamespaceVisit::printStack() throw(){
	Oscl::Queue<Oscl::Strings::Node>	tmp;
	Oscl::Strings::Node*				node;
	while((node=_stack.pop())){
		tmp.push(node);
		}
	while((node=tmp.pop())){
		fprintf(&_outputFile,"%s",node->getString());
		_stack.push(node);
		}
	}

void	NamespaceVisit::pushTab() throw(){
	Oscl::Strings::Node*	node
		= new Oscl::Strings::Node("\t");
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);
	}

void	NamespaceVisit::popTab() throw(){
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on stack\n");
		exit(1);
		}
	}

