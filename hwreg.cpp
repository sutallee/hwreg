#include <stdio.h>
#include <string.h>
#include "hwreg.h"

ParseNodeList*	theModuleDescList;

NameDesc::NameDesc() throw(){
	}

ParseNodeList::ParseNodeList() throw(){
	}

void ParseNodeList::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	for(ParseNode* node=first();node;node=next(node)){
		node->accept(visitor);
		}
	visitor.postVisit(*this);
	}

HardwareDesc::HardwareDesc() throw(){
	}

HardwareValueDesc::HardwareValueDesc(ParseNodeList* valueDescList) throw():
		_valueDescList(valueDescList)
		{
	}

void HardwareValueDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	_valueDescList->accept(visitor);
	visitor.postVisit(*this);
	}


HardwareRegisterDesc::HardwareRegisterDesc(ParseNodeList* regDescList) throw():
		_regDescList(regDescList)
		{
	}

void HardwareRegisterDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	_regDescList->accept(visitor);
	visitor.postVisit(*this);
	}

ModuleDesc::ModuleDesc(char* prefix,ParseNodeList* incDescList,ParseNodeList* hwDescList,ParseNodeList* namespaceDescList) throw():
		_hwDescList(hwDescList),
		_namespaceDescList(namespaceDescList),
		_incDescList(incDescList),
		_prefix(prefix)
		{
	}

void ModuleDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_incDescList) _incDescList->accept(visitor);
	if(_hwDescList) _hwDescList->accept(visitor);
	if(_namespaceDescList) _namespaceDescList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	ModuleDesc::getPrefix() throw(){
	return _prefix;
	}

NamespaceDesc::NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) throw():
		_parseNodeList(parseNodeList),
		_prefix(prefix)
		{
	}

void NamespaceDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	NamespaceDesc::getPrefix() throw(){
	return _prefix;
	}

ValueDesc::ValueDesc(char* symbol,int value) throw():
		_symbol(symbol),
		_value(value)
		{
	}

void ValueDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	ValueDesc::getSymbol() throw(){
	return _symbol;
	}

int					ValueDesc::getValue() throw(){
	return _value;
	}

RegisterDesc::RegisterDesc(char* symbol,Oscl::Strings::Dynamic* type,ParseNodeList* fieldSeqDescList) throw():
		_fieldSeqDescList(fieldSeqDescList),
		_symbol(symbol),
		_type(type)
		{
	}

void RegisterDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_fieldSeqDescList) _fieldSeqDescList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	RegisterDesc::getSymbol() throw(){
	return _symbol;
	}

const Oscl::Strings::Api&	RegisterDesc::getType() throw(){
	return *_type;
	}

FieldDesc::FieldDesc(char* symbol,int bitNum,int fieldWidth,ParseNodeList* valueDescList) throw():
		_valueDescList(valueDescList),
		_symbol(symbol),
		_bitNum(bitNum),
		_fieldWidth(fieldWidth)
		{
	}

void FieldDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_valueDescList) _valueDescList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	FieldDesc::getSymbol() throw(){
	return _symbol;
	}

int	FieldDesc::getBitNumber() throw(){
	return _bitNum;
	}

int	FieldDesc::getFieldWidth() throw(){
	return _fieldWidth;
	}

FieldSeqFieldDesc::FieldSeqFieldDesc(ParseNodeList* fieldDescList) throw():
		_fieldDescList(fieldDescList)
		{
	}

void FieldSeqFieldDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_fieldDescList) _fieldDescList->accept(visitor);
	visitor.postVisit(*this);
	}

FieldSeqValueDesc::FieldSeqValueDesc(ParseNodeList* valueDescList) throw():
		_valueDescList(valueDescList)
		{
	}

void FieldSeqValueDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	_valueDescList->accept(visitor);
	visitor.postVisit(*this);
	}

IncludeDesc::IncludeDesc(
			const char* prefix,
			const char* includeFile,
			const char* postfix
			) throw():
		_includeFile()
		{
	_includeFile	+= prefix;
	_includeFile	+= includeFile;
	_includeFile	+= postfix;
	}

void IncludeDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	IncludeDesc::getIncludeFile() const throw(){
	return _includeFile;
	}

StructDesc::StructDesc() throw(){
	}

void StructDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

UnionDesc::UnionDesc() throw(){
	}

void UnionDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

