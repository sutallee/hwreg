#ifndef _namespaceh_
#define _namespaceh_
#include <stdio.h>
#include "hwreg.h"
#include "src/oscl/strings/node.h"
#include "src/oscl/queue/queue.h"
#include "field.h"

/** */
class NamespaceVisit : public ParseElementVisitor {
	private:
		/** */
		Oscl::Queue<Oscl::Strings::Node>	_stack;
		/** */
		Oscl::Queue<FieldElement>			_fstack;
		/** */
		Oscl::Strings::Dynamic				_modulePrefix;
		/** */
		Oscl::Strings::Dynamic				_regPrefix;
		/** */
		Oscl::Strings::Dynamic				_fieldPrefix;
		/** */
		int									_bitNumber;
		/** */
		FILE&								_outputFile;
		/** */
		const bool							_useEnums;

	public:
		/** */
		NamespaceVisit(bool useEnums=false) throw();
		/** */
		NamespaceVisit(FILE& outputFile,bool useEnums=false) throw();
		/** */
		void	preVisit(ModuleDesc& moduleDesc) throw();
		/** */
		void	preVisit(ParseNodeList& parseNodeList) throw();
		/** */
		void	preVisit(IncludeDesc& incDesc) throw();
		/** */
		void	preVisit(NamespaceDesc& namespaceDesc) throw();
		/** */
		void	preVisit(HardwareValueDesc& hardwareValueDesc) throw();
		/** */
		void	preVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw();
		/** */
		void	preVisit(ValueDesc& valueDesc) throw();
		/** */
		void	preVisit(RegisterDesc& regDesc) throw();
		/** */
		void	preVisit(FieldDesc& fieldDesc) throw();
		/** */
		void	preVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw();
		/** */
		void	preVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw();
		/** */
		void	preVisit(FieldSeqList& fieldSeqList) throw();
		/** */
		void	preVisit(StructDesc& structDesc) throw();
		/** */
		void	preVisit(UnionDesc& unionDesc) throw();

		/** */
		void	postVisit(ModuleDesc& moduleDesc) throw();
		/** */
		void	postVisit(ParseNodeList& parseNodeList) throw();
		/** */
		void	postVisit(IncludeDesc& incDesc) throw();
		/** */
		void	postVisit(NamespaceDesc& namespaceDesc) throw();
		/** */
		void	postVisit(HardwareValueDesc& hardwareValueDesc) throw();
		/** */
		void	postVisit(HardwareRegisterDesc& hardwareRegisterDesc) throw();
		/** */
		void	postVisit(ValueDesc& valueDesc) throw();
		/** */
		void	postVisit(RegisterDesc& regDesc) throw();
		/** */
		void	postVisit(FieldDesc& fieldDesc) throw();
		/** */
		void	postVisit(FieldSeqFieldDesc& fieldSeqFieldDesc) throw();
		/** */
		void	postVisit(FieldSeqValueDesc& fieldSeqValueDesc) throw();
		/** */
		void	postVisit(FieldSeqList& fieldSeqList) throw();
		/** */
		void	postVisit(StructDesc& structDesc) throw();
		/** */
		void	postVisit(UnionDesc& unionDesc) throw();
	private:
		/** */
		void	printStack() throw();
		/** */
		void	pushTab() throw();
		/** */
		void	popTab() throw();
	};

#endif
